# "Saint John": what is writing to this log file?

## [Scenario Description](https://sadservers.com/scenario/saint-john)

Scenario: "Saint John": what is writing to this log file?

Level: Easy

Type: Fix

Tags:

Description: A developer created a testing program that is continuously writing to a log file `/var/log/bad.log` and filling up disk. You can check for example with `tail -f /var/log/bad.log`.
This program is no longer needed. Find it and terminate it.

Test: The log file size doesn't change (within a time interval bigger than the rate of change of the log file).

The "Check My Solution" button runs the script `/home/admin/agent/check.sh`, which you can see and execute.

Time to Solve: 10 minutes.

## Solution

There are a couple steps to be taken here in order to "sovle" the issue

1. check who is writing to the log file:

```bash
$ ps -ef | grep bad
admin        588       1  0 14:28 ?        00:00:00 /usr/bin/python3 /home/admin/badlog.py
admin        719     715  0 14:30 pts/1    00:00:00 grep bad
```

Process IDs may differ, but the important part is the actual Python script running `/home/admin/badlog.py` and that process id.

Kill the script sending the signal:

```bash
$ kill -9 588
```

After you kill the process check your solution.

Run the `/home/admin/agent/check.sh` script.

Soon, you'll find out that it has not solved the issue.

Turn to crontab to see if there's a culprit there:

```bash
$ crontab -l
#Ansible: reboot
@reboot /home/admin/badlog.py &
```

There it is. Comment that line and check your solution.

Run the check again.

Run the `/home/admin/agent/check.sh` script.

This time, you're done!
