#!/usr/bin/env python3
import json

with open("access.log","r") as fd:
    lines = fd.readlines()

counters = {}
for line in lines:
    ip = line.split(" ")[0]
    if ip in counters.keys():
        counters[ip] += 1
    else:
        counters[ip] = 1

# print(json.dumps(counters,indent=2))

max = 0
max_ip = "0.0.0.0"

for ip in counters.keys():
    # print(f"Processing {ip} with count: {counters[ip]} - Current Max IP {max_ip} with count: {max}")
    if counters[ip] > max:
        max = counters[ip]
        max_ip = ip

with open("highestip.txt","w") as fd:
    fd.write(f"{max_ip}\n")
