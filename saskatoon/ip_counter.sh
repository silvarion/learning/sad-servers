#!/usr/bin/env bash

# Variable initialization
max_count="0"
max_ip="0.0.0.0"

# Counting loop
for ip_address in $(cat access.log | cut -d" " -f1 | sort | uniq)
do
    ip_count=$(grep "$ip_address" access.log | wc -l)
    if [ "${ip_count}" -gt "${max_count}" ]
    then
        max_ip=${ip_address}
        max_count=${ip_count}
    fi
done

# Writing the answer
echo "$max_ip" > highestip.txt

rm -f temp_ips.txt
