# "Saskatoon": counting IPs

## [Scenario Description](https://sadservers.com/scenario/saskatoon)

**Scenario**: "Saskatoon": counting IPs.

**Level**: Easy

**Type**: Do

**Tags**: bash  

**Description**: There's a web server access log file at `/home/admin/access.log`. The file consists of one line per HTTP request, with the requester's IP address at the beginning of each line.

Find what's the IP address that has the most requests in this file (there's no tie; the IP is unique). Write the solution into a file /home/admin/highestip.txt. For example, if your solution is "1.2.3.4", you can do `echo "1.2.3.4" > /home/admin/highestip.txt`

**Test**: The SHA1 checksum of the IP address `sha1sum /home/admin/highestip.txt` is `6ef426c40652babc0d081d438b9f353709008e93` (just a way to verify the solution without giving it away.)

**Time to Solve**: 15 minutes.

## Solutions

### Bash Script

A simple `BASH` script can be used for this as show in the file [ip_counter.sh](ip_counter.sh):

```bash
#!/usr/bin/env bash

# Variable initialization
max_count="0"
max_ip="0.0.0.0"

# Counting loop
for ip_address in $(cat access.log | cut -d" " -f1 | sort | uniq) # Cut splits the line by spaces, get only the IP, then sort and grab only unique values
do
    ip_count=$(grep "$ip_address" access.log | wc -l)
    if [ "${ip_count}" -gt "${max_count}" ]
    then
        max_ip=${ip_address}
        max_count=${ip_count}
    fi
done

# Writing the answer
echo "$max_ip" > highestip.txt

rm -f temp_ips.txt
```

**Functions used**:

- `cat`: This function writes the contents of a file to standard output, one line at a time.
- `cut`: This function can split a line according to the delimiter `-d" "` and select the field `-f1` in the argument
- `sort`: Sorts the output to have the same IP addresses together
- `uniq`: removes the duplicated entries from the output

### AWK Script

Another option us to use AWK as shown in file [ip_counter.awk](ip_counter.awk):

```awk
#!/usr/bin/env -S awk -F " " -f ${_} --

BEGIN {
  delete ARGV[1]
  max_ip = 0
  max_count = 0
}

{count[$1]++} 

END {
  for (ip in count) {
#    printf "Processing %s with count %d\n",ip,count[ip]
    if (count[ip] > max_count) {
#      printf "new max found %d > %d\n",count[ip], max_count
      max_count = count[ip]
      max_ip = ip;
    }
  }
  print max_ip
}
```

Then just call redirect stdout to the highestip.txt file

```shell
./ipcounter.awk > highestip.txt
```

### Python Script

As an alternative, you could use Python3 as shown in file [ip_counter.py](ip_counter.py):

```python
#!/usr/bin/env python3
import json

with open("access.log","r") as fd:
    lines = fd.readlines()

counters = {}
for line in lines:
    ip = line.split(" ")[0]
    if ip in counters.keys():
        counters[ip] += 1
    else:
        counters[ip] = 1

# print(json.dumps(counters,indent=2))

max = 0
max_ip = "0.0.0.0"

for ip in counters.keys():
    # print(f"Processing {ip} with count: {counters[ip]} - Current Max IP {max_ip} with count: {max}")
    if counters[ip] > max:
        max = counters[ip]
        max_ip = ip

with open("highestip.txt","w") as fd:
    fd.write(f"{max_ip}\n")
```
