#!/usr/bin/env -S awk -F " " -f ${_} --

BEGIN {
  delete ARGV[1]
  max_ip = 0
  max_count = 0
}

{count[$1]++} 

END {
  for (ip in count) {
#    printf "Processing %s with count %d\n",ip,count[ip]
    if (count[ip] > max_count) {
#      printf "new max found %d > %d\n",count[ip], max_count
      max_count = count[ip]
      max_ip = ip;
    }
  }
  print max_ip
}