# Sad Servers

[Sad Servers Scenarios](https://sadservers.com/scenarios)

## Easy

| # | Name                                                                                                | Time | Type | Solution                                    |
|---|-----------------------------------------------------------------------------------------------------|------|------|---------------------------------------------|
| 1 | ["Saint John": what is writing to this log file?](https://sadservers.com/scenario/saint-john)       |  10m |  Fix |   [HERE](saint-john/solution.md)            |
| 2 | ["Saskatoon": counting IPs](https://sadservers.com/scenario/saskatoon)                              |  15m |  Do  |   [HERE](saskatoon/solution.md)             |
| 3 | ["Santiago": Find the secret combination](https://sadservers.com/scenario/santiago)                 |  15m |  Do  |   [HERE](santiago/solution.md)              |
| 4 | [The Command Line Murders](https://sadservers.com/scenario/command-line-murders)                    |  20m |  Do  |   [HERE](command-line-murders/solution.md)  |
| 5 | ["Taipei": Come a-knocking](https://sadservers.com/scenario/taipei)                                 |  15m | Hack |   [HERE]()   |
| 6 | ["Resumable Server": Linux Upskill Challenge](https://sadservers.com/scenario/luc)                  |  30m |  Do  |   [HERE]()   |
| 7 | ["Lhasa": Easy Math](https://sadservers.com/scenario/lhasa)                                         |  15m |  Do  |   [HERE]()   |
| 8 | ["Bucharest": Connecting to Postgres](https://sadservers.com/scenario/bucharest)                    |  10m |  Fix |   [HERE]()   |
| 9 | ["Bilbao": Basic Kubernetes Problems](https://sadservers.com/scenario/bilbao)                       |  10m |  Fix |   [HERE]()   |

## Medium

| #  | Name                                                                                               | Time | Type | Solution |
|----|----------------------------------------------------------------------------------------------------|------|------|----------|
| 10 | ["Manhattan": can't write data into database.](https://sadservers.com/scenario/manhattan)          |  20m |  Fix |   [HERE]()   |
| 11 | ["Tokyo": can't serve web file](https://sadservers.com/scenario/tokyo)                             |  15m |  Fix |   [HERE]()   |
| 12 | ["Cape Town": Borked Nginx](https://sadservers.com/scenario/capetown)                              |  15m |  Fix |   [HERE]()   |
| 13 | ["Salta": Docker container won't start](https://sadservers.com/scenario/salta)                     |  15m |  Fix |   [HERE]()   |
| 14 | ["Venice": Am I in a container?](https://sadservers.com/scenario/venice)                           |  15m |  Do  |   [HERE]()   |
| 15 | ["Oaxaca": Close an Open File](https://sadservers.com/scenario/oaxaca)                             |  15m |  Fix |   [HERE]()   |
| 16 | ["Melbourne": WSGI with Gunicorn](https://sadservers.com/scenario/melbourne)                       |  15m |  Fix |   [HERE]()   |
| 17 | ["Lisbon": etcd SSL cert troubles](https://sadservers.com/scenario/lisbon)                         |  15m |  Fix |   [HERE]()   |
| 18 | ["Kihei": Surely Not Another Disk Space Scenario](https://sadservers.com/scenario/kihei)           |  15m |  Fix |   [HERE]()   |
| 19 | ["Unimak Island": Fun with Mr Jason](https://sadservers.com/scenario/unimak)                       |  15m |  Fix |   [HERE]()   |
| 20 | ["Ivujivik": Parlez-vous Français?](https://sadservers.com/scenario/ivujivik)                      |  20m |  Do  |   [HERE]()   |
| 21 | ["Paris": Where is my webserver?](https://sadservers.com/scenario/paris)                           |  15m | Hack |   [HERE]()   |
| 22 | ["Buenos Aires": Kubernetes Pod Crashing](https://sadservers.com/scenario/buenos-aires)            |  20m |  Fix |   [HERE]()   |
| 23 | ["Tarifa": Between Two Seas](https://sadservers.com/scenario/tarifa)                               |  20m |  Fix |   [HERE]()   |
| 24 | ["Marrakech": Word Histogram](https://sadservers.com/scenario/marrakech)                           |  15m |  Do  |   [HERE]()   |

## Hard

| #  | Name                                                                                               | Time | Type | Solution |
|----|----------------------------------------------------------------------------------------------------|------|------|----------|
| 25 | ["Jakarta": it's always DNS](https://sadservers.com/scenario/jakarta)                              |  20m |  Fix |   [HERE]()   |
| 26 | ["Bern": Docker web container can't connect to db container](https://sadservers.com/scenario/bern) |  20m |  Fix |   [HERE]()   |
| 27 | ["Karakorum": WTFIT – What The Fun Is This?](https://sadservers.com/scenario/karakorum)            |  20m |  Fix |   [HERE]()   |
| 28 | ["Singara": Docker and Kubernetes web app not working](https://sadservers.com/scenario/singara)    |  20m |  Fix |   [HERE]()   |
| 29 | ["Hong-Kong": can't write data into database](https://sadservers.com/scenario/hongkong)            |  20m |  Fix |   [HERE]()   |
| 30 | ["Pokhara": SSH and other sshenanigans](https://sadservers.com/scenario/pokhara)                   |  30m |  Fix |   [HERE]()   |
| 31 | ["Roseau": Hack a Web Server](https://sadservers.com/scenario/roseau)                              |  30m | Hack |   [HERE]()   |
| 32 | ["Belo-Horizonte": A Java Enigma](https://sadservers.com/scenario/belo-horizonte)                  |  20m |  Fix |   [HERE]()   |
| 33 | ["Chennai": Pull a Rabbit from a Hat](https://sadservers.com/scenario/chennai)                     |  30m |  Fix |   [HERE]()   |
| 34 | ["Monaco": Disappearing Trick](https://sadservers.com/scenario/monaco)                             |  30m | Hack |   [HERE]()   |
