# "The Command Line Murders"

## [Scenario Description](https://sadservers.com/scenario/command-line-murders)

**Scenario**: "The Command Line Murders"

**Level**: Easy

**Type**: Do

**Tags**: bash  

**Description**: This is the Command Line Murders with a small twist as in the solution is different

Enter the name of the murderer in the file /home/admin/mysolution, for example echo "John Smith" > ~/mysolution

Hints are at the base of the /home/admin/clmystery directory. Enjoy the investigation!

Test: `md5sum ~/mysolution` returns `9bba101c7369f49ca890ea96aa242dd5`

(You can always see /home/admin/agent/check.sh to see how the solution is evaluated).

**Time to Solve**: 20 minutes

## Solution

Change the working directory to `/home/admin/clmystery`

Look for CLUEs in the `crimescene`.

```shell
grep CLUE mystery/crimescene

```

CLUE: Footage from an ATM security camera is blurry but shows that the perpetrator is a tall male, at least 6'.
CLUE: Found a wallet believed to belong to the killer: no ID, just loose change, and membership cards for Rotary_Club, Delta SkyMiles, the local library, and the Museum of Bash History. The cards are totally untraceable and have no name, for some reason.
CLUE: Questioned the barista at the local coffee shop. He said a woman left right before they heard the shots. The name on her latte was Annabel, she had blond spiky hair and a New Zealand accent.

```shell
grep Annabel mystery/people
```

    Annabel Sun     F       26      Hart Place, line 40
    Oluwasegun Annabel      M       37      Mattapan Street, line 173
    Annabel Church  F       38      Buckingham Place, line 179
    Annabel Fuglsang        M       40      Haley Street, line 176


To find the interviews 

```shell
head -40 mystery/streets/Hart_Place | tail -1
head -173 mystery/streets/Mattapan_Street | tail -1
head -179 mystery/streets/Buckingham_Place | tail -1
head -176 mystery/streets/Haley_Street | tail -1
```

To read the interviews

```shell
cat mystery/interviews/interview-47246024
cat mystery/interviews/interview-9437737
cat mystery/interviews/interview-699607
cat mystery/interviews/interview-871877
```
Interview 699607

    Interviewed Ms. Church at 2:04 pm.  Witness stated that she did not see anyone she could identify as the shooter, that she ran away as soon as the shots were fired.

    However, she reports seeing the car that fled the scene.  Describes it as a blue Honda, with a license plate that starts with "L337" and ends with "9"

Find the vehicles

```shell
grep -A 5 "L337" mystery/vehicles
```

    License Plate L337QE9
    Make: Honda
    Color: Blue
    Owner: Erika Owens
    Height: 6'5"
    Weight: 220 lbs
    --
    License Plate L337539
    Make: Honda
    Color: Blue
    Owner: Aron Pilhofer
    Height: 5'3"
    Weight: 198 lbs
    --
    License Plate L337369
    Make: Honda
    Color: Blue
    Owner: Heather Billings
    Height: 5'2"
    Weight: 244 lbs
    --
    License Plate L337DV9
    Make: Honda
    Color: Blue
    Owner: Joe Germuska
    Height: 6'2"
    Weight: 164 lbs
    --
    License Plate L3375A9
    Make: Honda
    Color: Blue
    Owner: Jeremy Bowers
    Height: 6'1"
    Weight: 204 lbs
    --
    License Plate L337WR9
    Make: Honda
    Color: Blue
    Owner: Jacqui Maher
    Height: 6'2"
    Weight: 130 lbs

Cross-check the records with all memberships.

```shell
cd mystery/memberships
cat Rotary_Club Delta_SkyMiles Terminal_City_Library Museum_of_Bash_History| grep -c "Joe Germuska"
```

Accuse the suspect

```shell
echo "Joe Germuska" > ~/mysolution
```
