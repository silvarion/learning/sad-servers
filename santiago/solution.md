# "Santiago": Find the secret combination

## [Scenario Description](https://sadservers.com/scenario/santiago)

**General Instructions**:

You have full access to a real Linux server (an ephemeral Virtual Machine) via an SSH session. (In some scenarios you can sudo, in others you can't except to sudo shutdown). Do whatever is necessary to fix the problem described so that a test passes within the alloted time. After that time the VM will be terminated.

As a constraint, from the servers you cannot go out to the Internet. DNS is available locally. Also do not interfere with services unrelated to the issue described that are needed for SadServers to work properly, specifically the services running on ports :2020 and :6767.

If you are stuck or not sure what to do, you can click on the "Next Clue / Solution" button and it will display a new hint (and the previous ones) until it reveals the solution. Close the clue window and click "Next Clue / Solution" again to get a new clue.

Once you think you have fixed the issue, click on the "Check My Solution" button to verify it as per the given Test.

To save the planet some CO2 emissions and also to reduce my AWS bill, servers are destroyed when the solution is detected correct with the "Check My Solution" button. If you give up before your time is up, you can shut off your server with: sudo shutdown -h now

**Scenario**: "Santiago": Find the secret combination

**Level**: Easy

**Description**: Alice the spy has hidden a secret number combination, find it using these instructions:

1) Find the number of lines with occurrences of the string Alice (case sensitive) in the *.txt files in the /home/admin directory
2) There's a file where Alice appears exactly once. In that file, in the line after that "Alice" occurrence there's a number.
Write both numbers consecutively as one (no new line or spaces) to the solution file. For example if the first number from 1) is 11 and the second 22, you can do echo -n 11 > /home/admin/solution; echo 22 >> /home/admin/solution or echo "1122" > /home/admin/solution.

**Test**: Running `md5sum /home/admin/solution` returns `d80e026d18a57b56bddf1d99a8a491f9` (just a way to verify the solution without giving it away.)

**Time to Solve**: 15 minutes.

**OS**: Debian 11

## Notes

This project uses the following free ebooks from [Project Guthenberg](https://www.gutenberg.org)

[11-0.txt - Alice's Adventures in Wonderland by Lewis Carroll](https://www.gutenberg.org/ebooks/11)

[1342-0.txt - Pride and Prejudice by Jane Austen](https://www.gutenberg.org/ebooks/1342)

[1661-0.txt - The Adventures of Sherlock Holmes by Arthur Conan Doyle](https://www.gutenberg.org/ebooks/1661)

[84-0.txt - Frankenstein; Or, The Modern Prometheus by Mary Wollstonecraft Shelley](https://www.gutenberg.org/ebooks/84)

## Solution

### Bash

A simple BASH script [count_alice.sh](count_alice.sh) can solve it

```bash
#!/usr/bin/env bash

echo -n "$(grep Alice *.txt | wc -l)" > solution

echo -n "$(grep -A 1 Alice 1342-0.txt | tail -1 | tr -s ' ' | cut -d' ' -f2)" >> solution
```
