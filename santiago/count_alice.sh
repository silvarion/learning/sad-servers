#!/usr/bin/env bash

echo -n "$(grep Alice *.txt | wc -l)" > solution

echo "$(grep -A 1 Alice 1342-0.txt | tail -1 | tr -s " " | cut -d" " -f2)" >> solution
