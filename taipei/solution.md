# "Taipei": Come a-knocking

## [Scenario Description](https://sadservers.com/scenario/taipei)

**Scenario**: "Taipei": Come a-knocking

**Level**: Easy

**Type**: Hack

**Tags**:

**Description**: There is a web server on port :80 protected with Port Knocking. Find the one "knock" needed (sending a SYN to a single port, not a sequence) so you can curl localhost.

**Test**: Executing curl localhost returns a message with md5sum fe474f8e1c29e9f412ed3b726369ab65. (Note: the resulting md5sum includes the new line terminator: echo $(curl localhost))

**Time to Solve**: 15 minutes.

## Solution

For this scenario, the `knock` utility comes handy

A simple shellscript can be used to _knock_ ports until the right one is touched.

See the [knocker.sh](knocker.sh) file contents below:

```shell
#!/usr/bin/env bash

current_port=1000
max_port=65535

while [ $current_port -le $max_port ]
do
    knock localhost $current_port -d 500
    try_curl=$(curl localhost | grep "Connection refused")
    if [ try_curl == "" ]
    then
        break
    fi
    current_port=$[$current_port+1000]
done
echo "Last knocked port: $current_port"
```

Executing this script on the host knocks ports in steps of 1000, it can be modified to use different steps, but for this particular scenario, it works like a charm.
