#!/usr/bin/env bash

current_port=1000
max_port=65535

while [ $current_port -le $max_port ]
do
    echo "Knocking port $current_port"
    knock localhost $current_port -d 500
    try_curl=$(curl localhost | grep "Connection refused")
    if [ try_curl == "" ]
    then
        break
    fi
    current_port=$[$current_port+1000]
done
echo "Last knocked port: $current_port"
