# "Bucharest": Connecting to Postgres

## [Scenario Description](https://sadservers.com/scenario/bucharest)

**Scenario**: "Bucharest": Connecting to Postgres

**Level**: Easy

**Type**: Fix

**Tags**: postgres  

**Description**: A web application relies on the PostgreSQL 13 database present on this server. However, the connection to the database is not working. Your task is to identify and resolve the issue causing this connection failure. The application connects to a database named app1 with the user app1user and the password app1user.

**Credit** [PykPyky](https://twitter.com/PykPyky)

**Test**: Running `PGPASSWORD=app1user psql -h 127.0.0.1 -d app1 -U app1user -c '\q'` succeeds (does not return an error).

**Time to Solve**: 10 minutes.

## Solution

Try to connect to the server with the test connection

```shell
PGPASSWORD=app1user psql -h 127.0.0.1 -d app1 -U app1user -c '\q'
```

The following error arises

    psql: error: FATAL:  pg_hba.conf rejects connection for host "127.0.0.1", user "app1user", database "app1", SSL on
    FATAL:  pg_hba.conf rejects connection for host "127.0.0.1", user "app1user", database "app1", SSL off


Edit the file `/etc/postgresql/13/main/pg_hba.conf`

```shell
sudo nano /etc/postgresql/13/main/pg_hba.conf
```

There are 2 entries that are rejecting all connections.

Comment the lines so that it looks like the following block

```ini
# Database administrative login by Unix domain socket
local   all             postgres                                peer
# host    all             all             all                     reject
# host    all             all             all                     reject
```

Login to the instance by changing to the `postgres` user and using `psql`

```shell
sudo su - postgres
psql
```

Reload the configuration using the following SQL command

```sql
SELECT pg_reload_conf();
```
